# Json to csv
Powered by:
[![Symfony](https://symfony.com/images/logos/header-logo.svg)](https://symfony.com/)

Json to csv is a console application made in symfony to download a jsonl file from aws s3 server and convert it into several file formats(xml, json, and csv[default]).
example usage:
```sh
$ php jsontocsv.php /*will export data to export.csv*/
$ php jsontocsv.php -f anyfilename.json /*will export data to json file*/
$ php jsontocsv.php -f anyfilename.xml /*will export data to xml file*/
```


## Contact Details
> Author: Putu Eka Doddy Suzanto (known as PED)

> email: northped@gmail.com phone: +6281236727676


### Technical decisions

Json to csv uses a number of open source projects to work properly:

* [Symfony](https://symfony.com/)
  > High Performance PHP framework
* [Composer](https://getcomposer.org/)
  > Dependency Manager for PHP
  - [aws/aws-sdk-php-symfony](https://packagist.org/packages/aws/aws-sdk-php-symfony) - A Symfony bundle for v3 of the AWS SDK for PHP
  - [linq/php-linq](https://packagist.org/packages/linq/php-linq) - The fast and modern library for easy data manipulation
  - [spatie/array-to-xml](https://packagist.org/packages/spatie/array-to-xml) - Convert an array to xml
  - [league/csv](https://packagist.org/packages/league/csv) - Csv data manipulation made easy in PHP
