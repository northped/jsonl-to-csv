<?
require_once __DIR__ . '/vendor/autoload.php';

use App\ExportCommand;
use Symfony\Component\Console\Application;

$application = new Application('Json to csv', '1.0.0');
$command = new ExportCommand();

$application->add($command);

$application->setDefaultCommand($command->getName(), true);
$application->run();