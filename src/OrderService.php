<?
namespace App;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class OrderService {
    public function get_orders() {
        $jsonl = $this->download_json();
        if ($jsonl == '') {
            return [];
        }

        $orders = $this->parse($jsonl);

        return $orders;
    }

    private function parse($jsonl) {
        $linq = \Linq\LinqFactory::createLinq();
        $isNotEmpty = function($item) {
            return trim($item);
        };
        $parseOrder = function($item) {
            $linq = \Linq\LinqFactory::createLinq();
            $order = json_decode($item);
            $itemsLinq = $linq->from($order->items);
            $order_id = $order->order_id;
            $order_datetime = $order->order_date;
            $total_order_value = array_sum($itemsLinq->select(
                function ($item) {
                    return $item->unit_price * $item->quantity; 
                })
            );
            $average_unit_price = array_sum($itemsLinq->select(
                function ($item) { 
                    return $item->unit_price; 
                })
            ) / $itemsLinq->count();
            $distinct_unit_count = count($itemsLinq->distinct(
                function ($item) { 
                    return $item->product->product_id; 
                })
            );
            $total_units_count = array_sum($itemsLinq->select(
                function ($item) {
                    return $item->quantity;
                })
            );

            $customer_state = $order->customer->shipping_address->state;
            $total_discount = 0;
            
            foreach($order->discounts as $discount) {
                if ($discount->type == "DOLLAR") {
                    $total_discount += $discount->value;
                } else {
                    if ($discount->priority == 1) {
                        $total_discount += $total_order_value * $discount->value / 100;
                    } else {
                        $total_discount += ($total_order_value - $total_discount) * $discount->value / 100;
                    }
                }
            }

            return array(
                "order_id" => $order_id,
                "order_datetime" => $order_datetime, 
                "total_order_value" => $total_order_value - $total_discount, 
                "average_unit_price" => $average_unit_price, 
                "distinct_unit_count" => $distinct_unit_count, 
                "total_units_count" => $total_units_count, 
                "customer_state" => $customer_state
            );
        };
        $jsons =  preg_split( '/\r\n|\r|\n/', $jsonl );
        $orders = $linq->from($jsons)->where($isNotEmpty)->select($parseOrder);

        return $orders;
    }

    private function download_json() {
        $bucket = 'catch-code-challenge';
        $keyname = 'challenge-1-in.jsonl';
        
        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'ap-southeast-2',
            'credentials' => false
        ]);
        
        try {
            $result = $s3->getObject([
                'Bucket' => $bucket,
                'Key'    => $keyname
            ]);
            return $result['Body'];
        } catch (S3Exception $e) {
            $output->writeln($e->getMessage() . PHP_EOL);
            return '';
        }
    }
}