<?
namespace App;
use League\Csv\Writer;
use Spatie\ArrayToXml\ArrayToXml;

class FileWriter {
    private $filename = '';
    private $format = '';

    function __construct($filename) {
        $this->filename = $filename;
        $this->set_format();
    }

    public function write($orders) {
        switch ($this->format) {
            case 'json':
                $this->write_file(json_encode($orders));

                break;
            case 'xml':
                $this->write_file(ArrayToXml::convert(array("orders" => $orders)));

                break;
            default:
                $this->write_csv($orders);
        }
    }

    private function set_format() {
        $filenames = explode('.', $this->filename ? $this->filename : 'export.csv');
        $this->format = count($filenames) > 1 || $filenames[1] ? $filenames[1] : 'csv';
        $this->filename = $filenames[0];
    }

    public function get_full_filename() {
        return $this->filename . '.' . $this->format;
    }

    function write_csv($orders) {
        $this->format = 'csv';
        $header = ["order_id","order_datetime","total_order_value","average_unit_price","distinct_unit_count","total_units_count","customer_state"];
        $csv = Writer::createFromString('');
        $csv->insertOne($header);
        $csv->insertAll($orders);
        $content = $csv->getContent();

        $this->write_file($content);
    }

    private function write_file($content) {
        $myfile = fopen($this->get_full_filename(), "w") or die("Unable to open file!");

        fwrite($myfile, $content);
        fclose($myfile);
    }
}