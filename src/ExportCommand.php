<?
namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCommand extends Command {
    protected function configure() {
        $this->setName('export')
            ->setDescription('Export json data from amazon s3 json to csv')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('filename', 'f', InputOption::VALUE_OPTIONAL),
                ])
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $file_writer = new FileWriter($input->getOption('filename'));
        $order_service = new OrderService();

        $orders = $order_service->get_orders();
        if (!$orders) {
            $output->writeln('Json file cannot be downloaded.');
        }

        $file_writer->write($orders);

        $output->writeln('Data has been exported to : ' .$file_writer->get_full_filename());
    }
}